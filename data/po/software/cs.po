# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-06-06 16:10+0000\n"
"Last-Translator: Martin Wimpress <code@flexion.org>\n"
"Language-Team: Czech (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/cs/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: cs\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: software.html:17
msgid "Software"
msgstr "Software"

#: software.html:17
msgid "Boutique"
msgstr ""

#: software.html20, 20
msgid "Search"
msgstr ""

#: software.html23, 23, 343
msgid "Boutique News"
msgstr ""

#: software.html32, 32, 138
msgid "Accessories"
msgstr ""

#: software.html33, 33, 151
msgid "Education"
msgstr ""

#: software.html34, 34, 164
msgid "Games"
msgstr "Hry"

#: software.html35, 35, 177
msgid "Graphics"
msgstr ""

#: software.html36, 36, 190
msgid "Internet"
msgstr ""

#: software.html37, 37, 203
msgid "Office"
msgstr ""

#: software.html38, 38, 216
msgid "Programming"
msgstr ""

#: software.html39, 39, 229
msgid "Sound & Video"
msgstr ""

#: software.html40, 40, 242
msgid "System Tools"
msgstr ""

#: software.html41, 41, 255
msgid "Universal Access"
msgstr ""

#: software.html42, 42, 265
msgid "Servers"
msgstr "Servery"

#: software.html43, 43
msgid "More Software"
msgstr ""

#: software.html44, 44
msgid "Fixes"
msgstr ""

#: software.html:53
msgid "Software Boutique"
msgstr "Software Boutique"

#: software.html:54
msgid ""
"There is an abundance of software available for Ubuntu MATE and some people "
"find that choice overwhelming. The Boutique is a carefully curated selection"
" of the best-in-class applications chosen because they integrate well, "
"complement Ubuntu MATE and enable you to self style your computing "
"experience."
msgstr ""

#: software.html:61
msgid "If you can't find what you're looking for, install one of the"
msgstr ""

#: software.html62, 360
msgid "Get More Apps"
msgstr ""

#: software.html:63
msgid "software centers"
msgstr ""

#: software.html:64
msgid "to explore the complete Ubuntu software catalog."
msgstr ""

#: software.html:68
msgid "More Apps Await."
msgstr ""

#: software.html:69
msgid ""
"Connect your computer to the Internet to explore a wide selection of "
"applications for Ubuntu MATE."
msgstr ""

#: software.html:71
msgid ""
"Once online, you'll be able to download and install tried & tested software "
"right here in Welcome. Our picks ensure that the software featured "
"integrates well the Ubuntu MATE desktop."
msgstr ""

#: software.html:77
msgid ""
"Sorry, Welcome could not establish a connection to the Ubuntu repository "
"server. Please check your connection and try again."
msgstr ""

#: software.html:82
msgid "Retry Connection"
msgstr ""

#: software.html:85
msgid "Connection Troubleshooting"
msgstr ""

#: software.html:86
msgid "Connection Help"
msgstr ""

#: software.html:92
msgid "Get Connected."
msgstr ""

#: software.html:93
msgid ""
"Here's a few things you can check, depending on the type of connection you "
"have:"
msgstr ""

#: software.html:94
msgid "Wired Connection"
msgstr ""

#: software.html:96
msgid "Is the cable securely plugged in?"
msgstr ""

#: software.html:97
msgid ""
"Is the router online and can other devices access the network? Try "
"restarting the router."
msgstr ""

#: software.html:100
msgid "Wireless Connection"
msgstr ""

#: software.html:101
msgid "Have you entered the correct wireless password from the"
msgstr ""

#: software.html:101
msgid "network indicator"
msgstr ""

#: software.html:101
msgid "in the upper-right?"
msgstr ""

#: software.html:103
msgid "When disconnected, the applet looks like this:"
msgstr ""

#: software.html:105
msgid ""
"Is there a physical switch that may have accidentally switched off wireless "
"functionality?"
msgstr ""

#: software.html:107
msgid "Is there a soft (keyboard) switch that toggles it off?"
msgstr ""

#: software.html:109
msgid "You may need to hold the FN key while pressing the function key."
msgstr ""

#: software.html:110
msgid "Here's an example:"
msgstr ""

#: software.html:114
msgid "Not working at all or experiencing sluggish connections?"
msgstr ""

#: software.html:115
msgid ""
"Sorry to hear that. You will need a temporary wired connection to install "
"working drivers."
msgstr ""

#: software.html:117
msgid "The"
msgstr ""

#: software.html:117
msgid "Additional Drivers"
msgstr ""

#: software.html:117
msgid "tab may have them available for your system."
msgstr ""

#: software.html:118
msgid ""
"Otherwise, you will need to manually install third party drivers for your "
"hardware, or a download specific package containing the firmware. See"
msgstr ""

#: software.html:119
msgid "Drivers"
msgstr "Ovladače"

#: software.html:119
msgid "in the"
msgstr ""

#: software.html:119
msgid "Getting Started"
msgstr "Začínáme"

#: software.html:120
msgid "section for more details."
msgstr ""

#: software.html:121
msgid "Feel free to"
msgstr ""

#: software.html:121
msgid "ask the community"
msgstr ""

#: software.html:121
msgid "if you need assistance."
msgstr ""

#: software.html:125
msgid "Our Picks"
msgstr ""

#: software.html:126
msgid "OK"
msgstr "OK"

#: software.html140, 153, 166, 179, 192, 205, 218, 231, 244
msgid "No Filter"
msgstr ""

#: software.html:142
msgid "Handy utilities for your computing needs."
msgstr ""

#: software.html:155
msgid "For study and children."
msgstr ""

#: software.html:168
msgid "A selection of 2D and 3D games for your enjoyment."
msgstr ""

#: software.html:181
msgid "For producing and editing works of art."
msgstr ""

#: software.html:194
msgid "For staying connected and enjoying the features of your own cloud."
msgstr ""

#: software.html:207
msgid "For more then just documents and spreadsheets."
msgstr ""

#: software.html:220
msgid "For the developers and system administrators out there."
msgstr ""

#: software.html:233
msgid "Multimedia software for listening and production."
msgstr ""

#: software.html:246
msgid "Software that makes the most out of your system resources."
msgstr ""

#: software.html:256
msgid "Software that makes your computer more accessible."
msgstr ""

#: software.html:266
msgid "One-click installations for serving the network."
msgstr ""

#: software.html:275
msgid "Discover More Software"
msgstr ""

#: software.html:276
msgid ""
"Graphical interfaces to browse a wide selection of software available for "
"your operating system."
msgstr ""

#: software.html:284
msgid "Miscellaneous Fixes"
msgstr ""

#: software.html:285
msgid ""
"This section contains operations that can fix common problems should you "
"encounter an error while upgrading or installing new software."
msgstr ""

#: software.html:292
msgid "Outdated Package Lists?"
msgstr ""

#: software.html:293
msgid "Your repository lists may be out of date, which can cause"
msgstr ""

#: software.html:293
msgid "Not Found"
msgstr ""

#: software.html:293
msgid "errors and"
msgstr ""

#: software.html:293
msgid "outdated version"
msgstr ""

#: software.html:294
msgid ""
"information when trying to install new or newer versions of software. This "
"is particularly the case when Ubuntu MATE connects online for the first time"
" after installation."
msgstr ""

#: software.html:297
msgid "Update the repository lists:"
msgstr ""

#: software.html:301
msgid "Update Sources List"
msgstr ""

#: software.html:302
msgid "Upgrade Installed Packages"
msgstr ""

#: software.html:306
msgid "Broken Packages?"
msgstr ""

#: software.html:307
msgid "When a previous installation or removal was interrupted"
msgstr ""

#: software.html:308
msgid "(for instance, due to power failure or loss of Internet connection)"
msgstr ""

#: software.html:309
msgid ""
"further software cannot be added or removed without properly re-configuring "
"these broken packages. If necessary, you may need to also resolve "
"dependencies and install any missing packages in order for the software to "
"run properly."
msgstr ""

#: software.html:315
msgid "Configure packages that were unpacked but not configured:"
msgstr ""

#: software.html:319
msgid "Download and install broken dependencies:"
msgstr ""

#: software.html:323
msgid "Configure Interrupted Packages"
msgstr ""

#: software.html:324
msgid "Resolve Broken Packages"
msgstr ""

#: software.html:328
msgid ""
"In addition, listed below are terminal equivalent commands that otherwise "
"appear in the"
msgstr ""

#: software.html:329
msgid "Software Updater"
msgstr ""

#: software.html:331
msgid "Upgrade all packages that have a new version available:"
msgstr ""

#: software.html:335
msgid "Upgrade to a new release of Ubuntu MATE:"
msgstr ""

#: software.html:344
msgid ""
"Keeping you informed of the changes made to Ubuntu MATE's Software Boutique."
msgstr ""

#: software.html:349
msgid "Search for Applications"
msgstr ""

#: software.html:350
msgid "Enter a keyword to search the Software Boutique."
msgstr ""

#: software.html:352
msgid "Name of Application"
msgstr ""

#: software.html:357
msgid "No software matched your key words."
msgstr ""

#: software.html:359
msgid "The Boutique is just a collection of software."
msgstr ""

#: software.html:361
msgid "Install a software center"
msgstr ""

#: software.html:363
msgid "to explore the entire Ubuntu catalogue."
msgstr ""

#: software.html:366
msgid "Search again and include proprietary software."
msgstr ""

#: software.html:380
msgid "Filter by Software License"
msgstr ""

#: software.html:381
msgid "Hide Proprietary Software"
msgstr ""

#: software.html:383
msgid "Show Terminal Commands"
msgstr ""

#: software.html:389
msgid "Proprietary Software"
msgstr ""

#: software.html:390
msgid ""
"is owned by an individual or company. There may be restrictions on its usage"
" (like a license agreement), and provides no access to the source code."
msgstr ""

#: software.html:393
msgid "Free and Open Source Software"
msgstr ""

#: software.html:394
msgid ""
"grants the user the freedom to share, study and modify the software without "
"restriction. For instance, the GPL is an example of a 'Free Software' "
"license."
msgstr ""

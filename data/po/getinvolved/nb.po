# 
# Translators:
# Kristoffer Ulseth <ulseth85@gmail.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-05-21 21:40+0000\n"
"Last-Translator: Kristoffer Ulseth <ulseth85@gmail.com>\n"
"Language-Team: Norwegian Bokmål (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/nb/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: nb\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: getinvolved.html:17
msgid "Get Involved"
msgstr "Bli Involvert"

#: getinvolved.html:25
msgid "Development"
msgstr "Utvikling"

#: getinvolved.html:26
msgid ""
"This is where you can get involved with Ubuntu MATE and MATE Desktop "
"development."
msgstr "Det er her du kan bli involvert med Ubuntu MATE og MATE Desktop utviklingen."

#: getinvolved.html:30
msgid ""
"It appears you are not connected to the Internet. Please check your "
"connection to access this content."
msgstr "Det ser ut til at du ikke er koblet til Internett. Kontroller tilkoblingen for å få tilgang til dette innholdet."

#: getinvolved.html:31
msgid "Sorry, Welcome was unable to establish a connection."
msgstr "Beklager, Velkommen var ikke i stand til å etablere en forbindelse."

#: getinvolved.html:32
msgid "Retry"
msgstr "Prøv igjen"

#: getinvolved.html:39
msgid "Ubuntu MATE Launchpad"
msgstr "Ubuntu MATE Launchpad"

#: getinvolved.html:47
msgid "Ubuntu MATE Bitbucket"
msgstr "Ubuntu MATE Bitbucket"

#: getinvolved.html:55
msgid "MATE Desktop GitHub"
msgstr "MATE Desktop GitHub"

#: getinvolved.html:62
msgid "Translations"
msgstr "Oversettelser"

#: getinvolved.html:65
msgid "MATE Desktop"
msgstr "MATE Skrivebord"

#: getinvolved.html:70
msgid "Ubuntu MATE Applications"
msgstr "Ubuntu MATE Programmer"

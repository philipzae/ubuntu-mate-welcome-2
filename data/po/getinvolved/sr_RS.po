# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-03-19 13:27+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Serbian (Serbia) (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/sr_RS/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sr_RS\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: getinvolved.html:17
msgid "Get Involved"
msgstr ""

#: getinvolved.html:24
msgid "Development"
msgstr ""

#: getinvolved.html:25
msgid ""
"This is where you can get involved with Ubuntu MATE and MATE Desktop "
"development."
msgstr ""

#: getinvolved.html:29
msgid ""
"It appears you are not connected to the Internet. Please check your "
"connection to access this content."
msgstr ""

#: getinvolved.html:30
msgid "Sorry, Welcome was unable to establish a connection."
msgstr ""

#: getinvolved.html:31
msgid "Retry"
msgstr ""

#: getinvolved.html:38
msgid "Ubuntu MATE Launchpad"
msgstr ""

#: getinvolved.html:46
msgid "Ubuntu MATE Bitbucket"
msgstr ""

#: getinvolved.html:54
msgid "MATE Desktop GitHub"
msgstr ""

#: getinvolved.html:61
msgid "Translations"
msgstr ""

#: getinvolved.html:64
msgid "MATE Desktop"
msgstr ""

#: getinvolved.html:69
msgid "Ubuntu MATE Applications"
msgstr ""
